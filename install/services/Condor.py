#!/usr/bin/env python

import common
from Configuration import Configuration
from Configuration import ConfigurationError
from Certificates  import Certificates
import VDTClient
#---------------------
import sys,os,os.path,string,time
import tarfile
import shutil
import pwd
import stat
import traceback


class Condor(Configuration):

  def __init__(self,inifile,ini_section,ini_options):
    self.ini_section = ini_section
    self.inifile     = inifile
    self.ini_options = ini_options
    Configuration.__init__(self,inifile)
    self.validate_section(ini_section,ini_options)

    self.condor_version      = None
    self.certificates        = None

    #--- secondary schedd files --
    self.schedd_setup_file   = "new_schedd_setup.sh"
    self.schedd_init_file    = "init_schedd.sh"
    self.schedd_startup_file = "start_master_schedd.sh"
    self.schedd_initd_function = "return # no secondary schedds"

  #----------------------------------
  # methods for returning attributes
  #----------------------------------
  #----------------------------------
  def install_vdt_client(self):
    return self.option_value(self.ini_section,"install_vdt_client")
  #----------------------------------
  def condor_location(self):
    return self.option_value(self.ini_section,"condor_location")
  #----------------------------------
  def condor_cfg(self):
    if self.split_condor_config() == "y":
      return self.condor_config_local()
    else:
      return self.condor_config()
  #----------------------------------
  def condor_local(self):
    return  "%s/condor_local" % self.condor_location()
  #----------------------------------
  def install_location(self):
    return self.option_value(self.ini_section,"install_location")
  #----------------------------------
  def glideinwms_location(self):
    return self.option_value(self.ini_section,"glideinwms_location")
  #----------------------------------
  def vdt_location(self):
    return self.option_value(self.ini_section,"vdt_location")
  #----------------------------------
  def x509_gsi_dn(self):
    return self.option_value(self.ini_section,"x509_gsi_dn")
  #---------------------
  def username(self):
    return self.option_value(self.ini_section,"username")
  #---------------------
  def service_name(self):
    return self.option_value(self.ini_section,"service_name")
  #---------------------
  def hostname(self):
    return self.option_value(self.ini_section,"hostname")
  #---------------------
  def condor_tarball(self):
    return self.option_value(self.ini_section,"condor_tarball")
  #---------------------
  def admin_email(self):
    return self.option_value(self.ini_section,"condor_admin_email")
  #---------------------
  def split_condor_config(self):
    return self.option_value(self.ini_section,"split_condor_config")
  #---------------------
  def gsi_location(self):
    return  self.option_value(self.ini_section,"cert_proxy_location")
  #---------------------
  def gsi_credential_type(self):
    return self.option_value(self.ini_section,"gsi_credential_type")
  #---------------------
  def condor_config(self):
    return "%s/etc/condor_config" % self.condor_location()
  #---------------------
  def condor_config_local(self):
    return "%s/condor_config.local" % (self.condor_local())
  #---------------------
  def initd_script(self):
    return  "%s/condor" % (self.condor_location())
  #---------------------
  def condor_mapfile(self):
    return "%s/certs/condor_mapfile" % self.condor_location()
  #---------------------
  def privilege_separation(self):
    if self.has_option(self.ini_section,"privilege_separation"):
      return self.option_value(self.ini_section,"privilege_separation")
    return "n"
  #---------------------
  def match_authentication(self):
    if self.has_option(self.ini_section,"match_authentication"):
      return self.option_value(self.ini_section,"match_authentication")
    return "n"
  #---------------------
  def number_of_schedds(self):
    option = "number_of_schedds"
    if not self.has_option(self.ini_section,option):
      return int(0)
    value =  self.option_value(self.ini_section,option)
    if common.not_an_integer(value):
      common.logerr("%s option is not a number: %s" % (option,value))
    return int(value)
  #----------------------------------
  def collector_port(self):
    option = "collector_port"
    if not self.has_option(self.ini_section,option):
      return int(0)
    value = self.option_value(self.ini_section,option)
    if common.not_an_integer(value):
      common.logerr("%s option is not a number: %s" % (option,value))
    return int(value)
  #---------------------
  def secondary_collectors(self):
    option = "number_of_secondary_collectors"
    if not self.has_option(self.ini_section,option):
      return int(0)
    value = self.option_value(self.ini_section,option)
    if common.not_an_integer(value):
      common.logerr("%s option is not a number: %s" % (option,value))
    return int(value)
  #---------------------
  def secondary_collector_ports(self):
    ports = []
    if self.secondary_collectors() == 0:
      return ports  # none
    collectors = int(self.secondary_collectors())
    for nbr in range(collectors):
      ports.append(int(self.collector_port()) + nbr + 1)
    return ports
  #--------------------------------
  def stop_condor(self):
    if self.client_only_install() == True:
      common.logerr( "This is a client only install. Nothing to stop.")
    else: 
      common.logit( "... stopping condor as user %s" % self.username())
      common.run_script("%s stop" % self.initd_script())
  #--------------------------------
  def start_condor(self):
    if self.client_only_install() == True:
      common.logerr( "This is a client only install. Nothing to start.")
    else:
      common.logit( "... starting condor as user %s" % self.username())
      common.run_script("%s start" % self.initd_script())
  #--------------------------------
  def restart_condor(self):
    if self.client_only_install() == True:
      common.logerr( "This is a client only install. Nothing to restart.")
    else:
      common.logit( "... restarting condor as user %s" % self.username())
      common.run_script("%s restart" % self.initd_script())
   
  #--------------------------------
  def client_only_install(self):
    if self.ini_section == "VOFrontend":
      return True
    return False

  #--------------------------------
  def configure_condor(self):
    common.logit( "\nCondor configuration starting\n")
    ## self.__setup_condor_env__()  # this is modifying all default profiles
    #-- put condor config in the environment 
    os.environ['CONDOR_CONFIG']="%s" % self.condor_config()
    #-- put the CONDOR_LOCATION/bin in the PATH for the rest of the installation
    if os.environ.has_key('PATH'):
      os.environ['PATH']="%s/bin:%s" % (self.condor_location(),os.environ['PATH'])
    else:
      os.environ['PATH']="%s/bin:" % self.condor_location()
    self.update_condor_config()
    self.configure_secondary_schedds()
    self.__create_initd_script__()
    common.logit( "\nCondor configuration completen")

  #--------------------------------
  def update_condor_config(self):
    common.logit( "\ncondor_config file update started")
    self.__setup_condor_config__()
    self.__update_condor_config_wms__()
    self.__update_condor_config_daemon__()
    self.configure_gsi_security()
    self.__update_condor_config_gsi__()
    self.__update_condor_config_schedd__()
    self.__update_condor_config_negotiator__()
    self.__update_condor_config_collector__()
    if self.ini_section == "WMSCollector":
      self.__update_condor_config_condorg__()
    self.update_condor_config_privsep()
    common.logit( "condor_config file update complete")

  #--------------------------------
  def install_certificates(self):
    """ Certificates are required for Condor GSI authentication. """
    certs = Certificates(self.inifile,self.ini_section)
    certs.install()
    self.certificates = certs.x509_cert_dir()

  #--------------------------------
  def install_vdtclient(self):
    if self.install_vdt_client() == "y":
      vdt = VDTClient.VDTClient(self.ini_section,self.inifile)
      vdt.install()
    else:
      common.logit("\n... VDT client install not requested.")

  #--------------------------------
  def validate_condor_install(self):
    common.logit( "\nVerifying Condor installation")
    common.validate_hostname(self.hostname())
    common.validate_user(self.username())
    common.validate_email(self.admin_email())
    common.validate_gsi(self.x509_gsi_dn(),self.gsi_credential_type(),self.gsi_location())
    self.__validate_collector_port__()
    self.__validate_secondary_collectors__()
    self.__validate_schedds__()
    self.__validate_condor_config__(self.split_condor_config())
    if "usercollector" not in self.colocated_services:
      self.__validate_tarball__(self.condor_tarball())
      common.validate_install_location(self.condor_location())

  #--------------------------------
  def __setup_condor_env__(self):
    sh_profile = """
#-- Condor settings --
if ! echo ${PATH} | grep -q %s/bin ; then
  PATH=${PATH}:%s/bin
fi
export CONDOR_CONFIG=%s
""" % (self.condor_location(),self.condor_location(),self.condor_config())

    csh_profile = """
#-- Condor settings --
set path = ( $path %s/bin )
setenv CONDOR_CONFIG %s
""" % (self.condor_location(),self.condor_config())

    if os.getuid()==0: # different if root or not
      self.__setup_root_condor_env__(sh_profile,csh_profile)
    else:
      self.__setup_user_condor_env__(sh_profile,csh_profile)

  #--------------------------------
  def __setup_root_condor_env__(self,sh_profile,csh_profile):
    common.logit("... creating an /etc/condor/condor_config link IS NOT BEING DONE")
    ## #JGW --- this could go away if no root installs ----
    ## common.logit( "... setting up condor environment as root")
    ## #--  Put link into /etc/condor/condor_config --
    ## if not os.path.exists('/etc/condor'):
    ##     common.logit( "... creating /etc/condor/condor_config")
    ##     os.mkdir('/etc/condor')
    ## if os.path.islink('/etc/condor/condor_config') or os.path.exists('/etc/condor/condor_config'):
    ##     common.logit("...  an old version exists... replace it")
    ##     os.unlink('/etc/condor/condor_config')
    ## os.symlink(self.condor_config(), '/etc/condor/condor_config')

    ## #--  put condor binaries in system wide path --
    ## filename = "/etc/profile.d/condor.sh"
    ## common.write_file("w",0644,filename,sh_profile) 
    ## filename = "/etc/profile.d/condor.csh"
    ## common.write_file("w",0644,filename,csh_profile) 

  #--------------------------------
  def __setup_user_condor_env__(self,sh_profile,csh_profile):
    common.logit( "... setting up condor environment as %s" % os.environ["LOGNAME"])
 
    common.logit("... appending to bashrc/cshrc scripts IS NOT BEING DONE")
    ##filename = "%s/.profile" % os.environ['HOME']
    ##common.write_file("a",0644,filename,sh_profile) 

    ##filename = "%s/.bashrc" % os.environ['HOME']
    ##common.write_file("a",0644,filename,sh_profile) 

    ##filename = "%s/.cshrc" % os.environ['HOME']
    ##common.write_file("a",0644,filename,csh_profile) 

    ##common.logit( "The Condor config has been put in your login files")
    ##common.logit( "Please remember to exit and reenter the terminal after the install")
 
  #--------------------------------
  def install_condor(self):
    common.logit("\nCondor installation starting\n")
    common.logit("... install location: %s" % (self.condor_location()))
    try:
      tar_dir="%s/tar" % (self.condor_location())
      if not os.path.isdir(tar_dir):
        os.makedirs(tar_dir)
    except Exception,e:
      common.logerr("Condor installation failed. Cannot make %s directory: %s" % (tar_dir,e))
    
    try:
        common.logit( "... extracting from tarball: %s" % self.condor_tarball())
        fd = tarfile.open(self.condor_tarball(),"r:gz")
        #-- first create the regular files --
        for f in fd.getmembers():
          if not f.islnk():
            fd.extract(f,tar_dir)
        #-- then create the links --
        for f in fd.getmembers():
          if f.islnk():
            os.link(os.path.join(tar_dir,f.linkname),os.path.join(tar_dir,f.name))
        fd.close()

        common.logit( "... running condor_configure")
        install_str="%s/condor-%s/release.tar" % (tar_dir,self.condor_version)
        if not os.path.isfile(install_str):
          # Condor v7 changed the packaging
          install_str="%s/condor-%s"%(tar_dir,self.condor_version)
        #if not os.path.isfile(install_str):
        #    common.logerr(("Cannot find path to condor_configure(%s)" % (install_str))
        cmdline="cd %(tar_dir)s/condor-%(version)s;./condor_configure --install=%(install_str)s --install-dir=%(condor_location)s --local-dir=%(condor_local)s --install-log=%(tar_dir)s/condor_configure.log" %  \
          {   "tar_dir": tar_dir,             "version": self.condor_version, 
          "install_str": install_str, "condor_location": self.condor_location(),
         "condor_local": self.condor_local(), }

        if os.getuid() == 0:
            cmdline="%s  --owner=%s" % (cmdline,self.username())
        common.run_script(cmdline)
    except Exception,e:
        shutil.rmtree(self.condor_location())
        common.logerr("Condor installation failed - %s" % (e))
    
    #--  installation files not needed anymore --
    shutil.rmtree(tar_dir)
    common.logit("\nCondor installation complete")

  #--------------------------------
  def __validate_schedds__(self):
    if self.daemon_list.find("SCHEDD") < 0:
      common.logit("... no schedds")
      return # no schedd daemon
    common.logit("... validating schedds: %s" % self.number_of_schedds())
    nbr = self.number_of_schedds()
    min = 0
    max = 99
    if nbr < min:
      common.logerr("number of schedds is negative: %s" % (nbr))
    if nbr > max:
      common.logerr("number of schedds exceeds maximum allowed value: %s" % (nbr))

  #--------------------------------
  def __validate_secondary_collectors__(self):
    if self.daemon_list.find("COLLECTOR") < 0:
      common.logit("... no secondary collectors")
      return # no collector daemon
    common.logit("... validating secondary collectors: %s" % self.secondary_collectors())
    nbr = self.secondary_collectors()
    min = 0
    max = 399
    if nbr < min:
      common.logerr("nbr of secondary collectors is negative: %s" % (nbr))
    if nbr > max:
      common.logerr("nbr of secondary collectors exceeds maximum allowed value: %s" % (nbr))
 
  #--------------------------------
  def __validate_collector_port__(self):
    if self.daemon_list.find("COLLECTOR") < 0:
      common.logit("... no collector")
      return # no collector daemon
    common.logit("... validating collector port: %s" % self.collector_port())
    port = self.collector_port()
    collector_port = 0
    min = 1
    max = 65535
    root_port = 1024
    if port < min:
      common.logerr("collector port option is negative: %s" % (port))
    if port > max:
      common.logerr("collector port option exceeds maximum allowed value: %s" % (port))
    if port < root_port:
      if os.getuid() == 0:  #-- root user --
        common.logit("Ports less that %i are generally reserved." % (root_port))
        common.logit("You have specified port %s for the collector." % (port))
        yn = raw_input("Do you really want to use privileged port %s? (y/n): "% port)
        if yn != 'y':
          common.logerr("... exiting at your request")
      else: #-- non-root user --
        common.logerr("Collector port (%s) less than %i can only be used by root." % (port,root_port))
 
  #--------------------------------
  def __validate_condor_config__(self,value):
    common.logit("... validating split_condor_config: %s" % value)
    if not value in ["y","n"]:
      common.logerr("Invalid split_condor_config value (%s)" % (value))

  #--------------------------------
  def __validate_tarball__(self,tarball):
    """ Attempts to verify that this is a valid Condor tarball.
        - the first level of the directory structure is the
          Condor release with a format 'condor-*'.
        - the tarball contains the condor-*/configure_condor script.
    """
    common.logit("... validating condor tarball: %s" % tarball)
    if not os.path.isfile(tarball):
      common.logerr("File (%s) not found" % (tarball))
    try:
      fd = tarfile.open(tarball,"r:gz")
    except:
      common.logerr("File (%s) not a valid tar.gz file" % (tarball))
    try:
        try:
            first_entry = fd.next().name
            if ( len(first_entry.split('/')) < 2 ):
              common.logerr("File (%s) is not a condor tarball! (found (%s), expected a subdirectory" % (tarball, first_entry))
            first_dir = first_entry.split('/')[0]+'/'
            if ( first_dir[:7] != "condor-"):
              common.logerr("File '%s' is not a condor tarball! (found '%s', expected 'condor-*/'" % (condor_tarball, first_dir))
            self.condor_version = first_dir[7:-1]
            common.logit( "... condor version: %s" % (self.condor_version))
            try:
                fd.getmember(first_dir + "condor_configure")
            except:
                common.logerr("Condor tarball (%s) missing %s" % (tarball, first_dir + "condor_configure"))
        except Exception,e:
            common.logerr("Condor tarball file is corrupted: %s" % (tarball))
    finally:
      fd.close()

  #--------------------------------
  def __create_condor_mapfile__(self,mapfile_entries=None):
    """ Creates the condor mapfile for GSI authentication"""
    if mapfile_entries == None:
      common.logit( "... No Condor mapfile file needed or entries specified.")
      return
    filename = self.condor_mapfile()
    common.logit("... creating Condor mapfile: %s" % filename)
    common.make_directory(os.path.dirname(filename),pwd.getpwuid(os.getuid())[0],0755,empty_required=False)
    mapfile_entries += """GSI (.*) anonymous
FS (.*) \\1
""" 
    common.write_file("w",0644,filename,mapfile_entries)
    common.logit("... condor mapfile entries:")
    common.logit(os.system("cat %s" % filename))
    common.logit("... creating Condor mapfile complete.\n")

  #--------------------------------
  def __setup_condor_config__(self):
    """ If we are using a condor_config.local, then we will be populating the
        the one in condor_local
    """
    if len(self.colocated_services) > 0:
      return  # we've already updated this
    common.logit("... updating %s" % self.condor_config())
    if self.split_condor_config() == "y":
      #--- point the regular config to the local one ---
      cfg_data = """
########################################################
# Using local configuration file below
########################################################
LOCAL_CONFIG_FILE = %s
""" % (self.condor_config_local())
      common.logit( "    to use: %s" % (self.condor_config_local()))
    else: 
      #-- else always update the main config --
      cfg_data = """
########################################################
# disable additional config files 
########################################################
LOCAL_CONFIG_FILE = 
"""
      common.logit( "   to use: %s" % (self.condor_config()))

    #-- update the main config ---
    common.write_file("a",0644,self.condor_config(),cfg_data)

  #--------------------------------
  def __update_condor_config_wms__(self):
    if len(self.colocated_services) > 0:
      return  # we've already updated for these common attributes
    data = self.__condor_config_wms_data__()
    self.__append_to_condor_config__(data,"glideinWMS data")

  #--------------------------------
  def __update_condor_config_gsi__(self):
    if len(self.colocated_services) > 0:
      return  # we've already updated for these common attributes
    data = self.__condor_config_gsi_data__()
    self.__append_to_condor_config__(data,"GSI")

  #-----------------------------
  def __update_gsi_daemon_names__(self,x509_gsi_dns):
    if len(self.colocated_services) > 0:
      return  # we've already updated for these common attributes
    if len(x509_gsi_dns) == 0:
      common.logit("... no GSI_DAEMON_NAMEs to add")
      return
    data =  """
#####################################
# Whitelist of condor daemon DNs
#####################################
%s
""" % (x509_gsi_dns)
    self.__append_to_condor_config__(data,"GSI_DAEMON_NAME")


  #--------------------------------
  def __update_condor_config_daemon__(self):
    data = self.__condor_config_daemon_data__()
    self.__append_to_condor_config__(data,"DAEMON")

  #--------------------------------
  def __update_condor_config_negotiator__(self):
    if self.daemon_list.find("NEGOTIATOR") >= 0:
      data = self.__condor_config_negotiator_data__()
      self.__append_to_condor_config__(data,"NEGOTIATOR")

  #--------------------------------
  def __update_condor_config_schedd__(self):
    if self.daemon_list.find("SCHEDD") >= 0:
      data = self.__condor_config_schedd_data__()
      #-- checking for zero swap space - affects schedd's only --
      rtn = os.system("free | tail -1 |awk '{ if ( $2 == 0 ) {exit 0} else {exit 1} }'")
      if rtn == 0:
        data = data + """
################
# No swap space 
################
RESERVED_SWAP = 0
"""
      self.__append_to_condor_config__(data,"SCHEDD")


  #--------------------------------
  def __update_condor_config_collector__(self):
    if self.daemon_list.find("COLLECTOR") >= 0:
      data = self.__condor_config_collector_data__()
      data = data + self.__condor_config_secondary_collector_data__()
    else: # no collector, identifies one to use
      data = """
####################################
# Collector for user submitted jobs
####################################
CONDOR_HOST = %(host)s
COLLECTOR_HOST = $(CONDOR_HOST):%(port)s
""" % { "host" : self.option_value("UserCollector","hostname"),
        "port" : self.option_value("UserCollector","collector_port"),
      }
    self.__append_to_condor_config__(data,"COLLECTOR")

  #--------------------------------
  def __update_condor_config_condorg__(self):
    data = self.__condor_config_condorg_data__()
    self.__append_to_condor_config__(data,"CONDOR-G")

  #--------------------------------
  def update_condor_config_privsep(self):
    data = self.condor_config_privsep_data()
    self.__append_to_condor_config__(data,"Privilege Separation")

  #--------------------------------
  def __append_to_condor_config__(self,data,type):
    common.logit("... updating condor_config: %s entries" % type)
    if self.split_condor_config() == "y":
      common.write_file("a",0644,self.condor_config_local(),data)
    else:
      common.write_file("a",0644,self.condor_config(),data)

  #--------------------------------
  def __create_initd_script__(self):
    if self.client_only_install() == True:
      common.logit("... client only install. No startup initd script required.")
      return
    common.logit("\nCreating startup initd script")
    if os.path.exists(self.initd_script()):
      common.logit("... startup script already exists: %s" % self.initd_script())
      common.logit("... overwritting it")
    data = self.__initd_script__()
    common.write_file("w",0755,self.initd_script(),data)

  #----------------------------------
  def configure_secondary_schedds(self):
    common.logit("\nConfiguring secondary schedd support.")
    if self.daemon_list.find("SCHEDD") < 0:
      common.logit("... no schedds daemons for this condor instance")
      return
    if self.number_of_schedds() == 0:
      common.logit("... no secondary schedds to configure")
      return
    self.__create_secondary_schedd_support_files__()
    self.schedd_initd_function = ""
    schedds = int(self.number_of_schedds())
    for i in range(schedds):
      schedd_name = "%s%i" % (self.schedd_name_suffix,i+1)
      #-- run the init script --
      user = pwd.getpwnam(self.username())
      condor_ids = "%s.%s" % (user[2],user[3])
      common.run_script("export CONDOR_IDS=%s;%s/%s %s" % (condor_ids,self.condor_location(),self.schedd_init_file,schedd_name))
      #-- add the start script to the condor initd function --
      data = "  $CONDOR_LOCATION/%s %s" % (self.schedd_startup_file,schedd_name)
      self.schedd_initd_function = "%s\n%s" % (self.schedd_initd_function,data)
    #-- recreate the initd script with the function --
    common.logit("\nConfiguring secondary schedd support complete.\n")

  #----------------------------------
  def __create_secondary_schedd_support_files__(self):
    common.logit("... creating secondary schedd support files")
    filename = "%s/%s" % (self.condor_location(),self.schedd_setup_file)
    data = self.__secondary_schedd_setup_file_data__()
    common.write_file("w",0644,filename,data)

    filename = "%s/%s" % (self.condor_location(),self.schedd_init_file)
    data = self.__secondary_schedd_init_file_data__()
    common.write_file("w",0755,filename,data)

    filename = "%s/%s" % (self.condor_location(),self.schedd_startup_file)
    data = self.__secondary_schedd_startup_file_data__()
    common.write_file("w",0755,filename,data)
    common.logit("... creating secondary schedd support files complete")

  #----------------------------------
  def __secondary_schedd_setup_file_data__(self):
    return """\
if [ $# -ne 1 ]
then
 echo "ERROR: arg1 should be schedd name."
 return 1
fi

LD=%s
export _CONDOR_SCHEDD_NAME=schedd_$1
export _CONDOR_MASTER_NAME=${_CONDOR_SCHEDD_NAME}
# SCHEDD and MASTER names MUST be the same (Condor requirement)
export _CONDOR_DAEMON_LIST="MASTER,SCHEDD"
export _CONDOR_LOCAL_DIR=$LD/$_CONDOR_SCHEDD_NAME
export _CONDOR_LOCK=$_CONDOR_LOCAL_DIR/lock
unset LD
""" % (self.condor_local())

  #----------------------------------
  def __secondary_schedd_init_file_data__(self):
    return """\
#!/bin/sh
CONDOR_LOCATION=%s
script=$CONDOR_LOCATION/%s
source $script $1
if [ "$?" != "0" ];then
  echo "ERROR in $script"
  exit 1
fi
# add whatever other config you need
# create needed directories
$CONDOR_LOCATION/sbin/condor_init
""" % (self.condor_location(),self.schedd_setup_file)

  #----------------------------------
  def __secondary_schedd_startup_file_data__(self):
    return """\
#!/bin/sh
CONDOR_LOCATION=%s
export CONDOR_CONFIG=$CONDOR_LOCATION/etc/condor_config
source $CONDOR_LOCATION/new_schedd_setup.sh $1
# add whatever other config you need
$CONDOR_LOCATION/sbin/condor_master
""" % (self.condor_location())

  #----------------------------------
  def __initd_script__(self):
    data = """#!/bin/sh
# condor   This is the Condor batch system
# chkconfig: 35 90 30
# description: Starts and stops Condor

# Source function library.
if [ -f /etc/init.d/functions ] ; then
  . /etc/init.d/functions
elif [ -f /etc/rc.d/init.d/functions ] ; then
  . /etc/rc.d/init.d/functions
else
  exit 0
fi

#-- Condor settings --
CONDOR_LOCATION=%(condor_location)s
if [ ! -d "$CONDOR_LOCATION" ];then
  echo "ERROR: CONDOR_LOCATION does not exist: $CONDOR_LOCATION"
  failure
  exit 1
fi
if [ ! `echo ${PATH} | grep -q $CONDOR_LOCATION/bin` ]; then
  PATH=${PATH}:$CONDOR_LOCATION/bin
fi
export CONDOR_CONFIG=$CONDOR_LOCATION/etc/condor_config
if [ ! -f "$CONDOR_CONFIG" ];then
  echo "ERROR: CONDOR_CONFIG not found: $CONDOR_CONFIG"
  failure
  exit 1
fi
CONDOR_MASTER=$CONDOR_LOCATION/sbin/condor_master
if [ ! -f "$CONDOR_MASTER" ];then
  echo "ERROR: cannot find $CONDOR_MASTER"
  failure
  exit 1
fi

#---- verify correct user is starting/stopping condor --
validate_user () {
  config_vals="GSI_DAEMON_CERT GSI_DAEMON_KEY GSI_DAEMON_PROXY"
  good="no"
  for gsi_type in $config_vals
  do
    file="$(condor_config_val $gsi_type 2>/dev/null)"
    if [ ! -z "$file" ];then
      if [ -r "$file" ];then
        owner=$(ls -l $file | awk '{print $3}')
        me="$(/usr/bin/whoami)"
        if [ "$me" = "$owner" ];then
          good="yes"
          break
        fi
      fi
    fi
  done
  if [ $good = "no" ];then
    echo "ERROR: GSI self authentication will fail." 
    echo "Check these Condor attributes and verify ownership."
    echo "    $config_vals"
    echo "Or you may be starting/stopping Condor as the wrong user."
    if [ -n "$owner" ];then
      echo "You should be starting  as user: $owner"
      echo "You are trying to start as user: $me"
    fi
    exit 1
  fi
}

#---- secondary schedd start function ---
start_secondary_schedds () {
%(schedds)s
}

#-- start --
start () { 
   validate_user
   condor_status
   [ "$RETVAL" = "0" ] && { echo "Condor is already running";return
}
   echo -n "Starting condor: "
   $CONDOR_MASTER 2>/dev/null 1>&2 && success || failure
   RETVAL=$?
   start_secondary_schedds
   echo
   sleep 3
   condor_status
   [ "$RETVAL" != "0" ] && { echo "ERROR: Condor did not start correctly"
}
}

#-- stop --
stop () { 
   validate_user
   condor_status
   [ "$RETVAL" != "0" ] && { return
}
   echo -n "Shutting down condor: "
   killall -q -15 -exact $CONDOR_MASTER && success || failure
   sleep 3
   condor_status
   [ "$RETVAL" != "0" ] && { return  # the stop worked
}
   echo -n "Shutting down condor with SIGKILL: "
   # If a master is still alive, we have a problem
   killall -q -9 -exact $CONDOR_MASTER && success || failure
   condor_status
   RETVAL=$?
   [ "$RETVAL" != "0" ] && { return  # the stop worked
}
}
#-- restart --
restart () { 
   stop
   start
}

#-- status --
condor_status () { 
   pids="$(ps -ef |grep $CONDOR_MASTER |egrep -v grep | awk '{printf "%(format)s ", $2}')"
   echo
   if [ -z "$pids" ];then
     echo "$CONDOR_MASTER not running"
     RETVAL=1
   else
   echo "$CONDOR_MASTER running...
pids ($pids)"
     RETVAL=0
   fi
   echo
}

#--------
prog=$(basename $0)

case $1 in
   start  ) start ;;
   stop   ) stop ;;
   restart) restart ;;
   status ) condor_status ;;
        * ) echo "Usage: $prog {start|stop|restart|status}"; exit 1 ;;
esac
exit $RETVAL
""" % { "condor_location" : self.condor_location(),
        "schedds" : self.schedd_initd_function,
        "format" : "%s",
      }
    return data

  #-----------------------------
  def __condor_config_wms_data__(self):
    return  """
######################################################
# Base configuration values for glideinWMS
######################################################
##  Contact (via email) when problems occur
CONDOR_ADMIN = %s
##########################################
#  With glideins, there is nothing shared
##########################################
UID_DOMAIN=$(FULL_HOSTNAME)
FILESYSTEM_DOMAIN=$(FULL_HOSTNAME)

####################################################################
#  Condor needs to create a few lock files to synchronize access to 
#  various log files.  Use the log directory so they are collocated
####################################################################
LOCK = $(LOG)

############################################################
## Security config
############################################################
############################
# Authentication settings
############################
SEC_DEFAULT_AUTHENTICATION = REQUIRED
SEC_DEFAULT_AUTHENTICATION_METHODS = FS
SEC_READ_AUTHENTICATION = OPTIONAL
SEC_CLIENT_AUTHENTICATION = OPTIONAL
DENY_WRITE = anonymous@*
DENY_ADMINISTRATOR = anonymous@*
DENY_DAEMON = anonymous@*
DENY_NEGOTIATOR = anonymous@*
DENY_CLIENT = anonymous@*
#
############################
# Privacy settings
############################
SEC_DEFAULT_ENCRYPTION = OPTIONAL
SEC_DEFAULT_INTEGRITY = REQUIRED
SEC_READ_INTEGRITY = OPTIONAL
SEC_CLIENT_INTEGRITY = OPTIONAL
SEC_READ_ENCRYPTION = OPTIONAL
SEC_CLIENT_ENCRYPTION = OPTIONAL
""" % (self.admin_email())


  #-----------------------------
  def __condor_config_gsi_data__(self):
    data = ""
    data =  data + """
############################################################
## GSI Security config
############################################################
############################
# Authentication settings
############################
SEC_DEFAULT_AUTHENTICATION_METHODS = FS,GSI

############################
# Grid Certificate directory
############################
GSI_DAEMON_TRUSTED_CA_DIR=%s
""" % (self.certificates)

    if self.gsi_credential_type() == "proxy":
      data = data + """
############################
# Credentials
############################
GSI_DAEMON_PROXY = %s 
""" % self.gsi_location()
    else:
      data = data + """
############################
# Credentials
############################
GSI_DAEMON_CERT = %s
GSI_DAEMON_KEY  = %s
""" % (self.gsi_location(),string.replace(self.gsi_location(),"cert.pem","key.pem"))

    if self.condor_version >= "7.4":
      data = data + """
#####################################################
# With strong security, do not use IP based controls
#####################################################
HOSTALLOW_WRITE = *
ALLOW_WRITE = $(HOSTALLOW_WRITE)
"""
    else:
      data = data + """
#####################################################
# With strong security, do not use IP based controls
#####################################################
HOSTALLOW_WRITE = *
"""

    data = data + """
############################
# Set daemon cert location
############################
GSI_DAEMON_DIRECTORY = %s

#################################
# Where to find ID->uid mappings
#################################
CERTIFICATE_MAPFILE=%s
""" % ( os.path.dirname(self.condor_mapfile()),self.condor_mapfile())

    return data


  #-----------------------------
  def __condor_config_daemon_data__(self):
    data = ""
    if len(self.colocated_services) == 0:
      data =  data + """
######################################################
## daemons
######################################################
DAEMON_LIST   = MASTER
DAEMON_LIST   = $(DAEMON_LIST), %s""" % self.daemon_list
    else: 
      data =  data + """
######################################################
## daemons (adding for co-located services)
######################################################
DAEMON_LIST   = $(DAEMON_LIST), %s """ % self.daemon_list

    if self.client_only_install() == True and \
       len(self.colocated_services) == 0:
      data = data + """
#-- This machine should run no daemons
DAEMON_SHUTDOWN = True
"""
    else:
      data = data + """
#####################################
# Limit session caching to ~12h
#####################################
SEC_DAEMON_SESSION_DURATION = 50000

##########################################################
# Prepare the Shadow for use with glexec-enabled glideins
##########################################################
SHADOW.GLEXEC_STARTER = True
SHADOW.GLEXEC = /bin/false
""" 
      if self.match_authentication() == "y":
        data = data + """
#####################################
# Enable match authentication
#####################################
SEC_ENABLE_MATCH_PASSWORD_AUTHENTICATION=TRUE
""" 
        if self.condor_version <= "7.5.3":
          data = data + """
#############################################################
# Enable match authentication workaround for Condor ticket
# https://condor-wiki.cs.wisc.edu/index.cgi/tktview?tn=1481
#############################################################
SHADOW_WORKLIFE = 0
""" 
    return data

  #-----------------------------
  def __condor_config_schedd_data__(self):
    data =  """
######################################################
## Schedd tuning
######################################################
#--  Allow up to 6k concurrent running jobs
MAX_JOBS_RUNNING        = 6000
#--  Start max of 50 jobs every 2 seconds
JOB_START_DELAY = 2
JOB_START_COUNT = 50
#--  Stop 30 jobs every seconds
#--  This is needed to prevent glexec overload, when used
#--  Works for Condor v7.3.1 and up only, but harmless for older versions
JOB_STOP_DELAY = 1
JOB_STOP_COUNT = 30

#--  Raise file transfer limits
#--  no upload limits, since JOB_START_DELAY limits that
MAX_CONCURRENT_UPLOADS = 0
#--  but do limit downloads, as they are asyncronous
MAX_CONCURRENT_DOWNLOADS = 100

#--  Prevent checking on ImageSize
APPEND_REQ_VANILLA = (Memory>=1) && (Disk>=1)

#--  Prevent preemption
MAXJOBRETIREMENTTIME = $(HOUR) * 24 * 7
#-- GCB optimization
SCHEDD_SEND_VACATE_VIA_TCP = True
STARTD_SENDS_ALIVES = True
#-- Reduce disk IO - paranoid fsyncs are usully not needed
ENABLE_USERLOG_FSYNC = False
"""
    return data

  #-----------------------------
  def __condor_config_negotiator_data__(self):
    data = """
###########################################################
# Negotiator tuning
###########################################################
#-- Prefer newer claims as they are more likely to be alive
NEGOTIATOR_POST_JOB_RANK = MY.LastHeardFrom
#-- Increase negotiation frequency, as new glideins do not trigger a reschedule
NEGOTIATOR_INTERVAL = 60
NEGOTIATOR_MAX_TIME_PER_SUBMITTER=40
NEGOTIATOR_MAX_TIME_PER_PIESPIN=20
#-- Prevent preemption
PREEMPTION_REQUIREMENTS = False
#-- negotiator/GCB optimization
NEGOTIATOR_INFORM_STARTD = False
#-- Disable VOMS checking
NEGOTIATOR.USE_VOMS_ATTRIBUTES = False
"""
    if self.ini_section == "UserCollector":
      data = data + """
#-- Causes Negotiator to run faster. PREEMPTION_REQUIREMENTS and all 
#-- condor_startd rank expressions must be alse for 
#-- NEGOTIATOR_CONSIDER_PREEMPTION to be False
NEGOTIATOR_CONSIDER_PREEMPTION = False
"""
    return data

  #-----------------------------
  def __condor_config_collector_data__(self):
    data = """
###########################################################
# Collector Data
###########################################################
COLLECTOR_NAME = %(name)s
COLLECTOR_HOST = $(CONDOR_HOST):%(port)s
#-- disable VOMS checking
COLLECTOR.USE_VOMS_ATTRIBUTES = False
""" % {"name":self.service_name(), "port":self.collector_port()}
    return data

  #-----------------------------
  def __condor_config_secondary_collector_data__(self):
    if self.secondary_collectors() == 0:
      return ""  # none
    data = """
#################################################
# Secondary collectors
#################################################"""
#-- define sub-collectors, ports and log files
    for nbr in range(int(self.secondary_collectors())):
      data = data + """
COLLECTOR%(nbr)i = $(COLLECTOR)
COLLECTOR%(nbr)i_ENVIRONMENT = "_CONDOR_COLLECTOR_LOG=$(LOG)/Collector%(nbr)iLog"
COLLECTOR%(nbr)i_ARGS = -f -p %(port)i
""" % {"nbr":nbr, "port":self.secondary_collector_ports()[nbr]}

    data = data + "\n#-- Add subcollectors to the list of daemons to start\n"
    for nbr in range(int(self.secondary_collectors())):
      data = data + "DAEMON_LIST = $(DAEMON_LIST), COLLECTOR%i\n" % nbr

    data = data + """
#-- Forward ads to the main collector
#-- (this is ignored by the main collector, since the address matches itself)
CONDOR_VIEW_HOST = $(COLLECTOR_HOST)
"""
    return data

  #-----------------------------
  def __condor_config_condorg_data__(self):
    data = """
######################################################
## Condor-G tuning
######################################################
GRIDMANAGER_LOG = /tmp/GridmanagerLog.$(SCHEDD_NAME).$(USERNAME)
GRIDMANAGER_MAX_SUBMITTED_JOBS_PER_RESOURCE=5000
GRIDMANAGER_MAX_PENDING_SUBMITS_PER_RESOURCE=5000
GRIDMANAGER_MAX_PENDING_REQUESTS=500
"""
    return data

  #------------------------------------------
  #-- Must be populated in top level class --
  #-- if Privilege Separation used         --
  #------------------------------------------
  def condor_config_privsep_data(self):
    return ""

#--- end of Condor class ---------
####################################

def main(argv):
  try:
    print argv
    inifile = "/home/weigand/glidein-ini/glidein-all-xen21-doug.ini"
    section = "WMSCollector"
    options = {}
    condor = Condor(inifile,section,options)
    #condor.install_condor()
    condor.__validate_tarball__("/usr/local/tarballs/" + argv[1])
  except ConfigurationError, e:
    print "ERROR: %s" % e;return 1
  except common.WMSerror, e:
    print "WMSError";return 1
  except Exception, e:
    print traceback.print_exc()
  return 0



#--------------------------
if __name__ == '__main__':
  sys.exit(main(sys.argv))

    
